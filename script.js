/* init */
var plr=true;                                   // true:p1(b),false:p2(w)
var ovr=false;                                  // game over status
var grid=new Array();                           // the grid
var rows=12;                                    // rows (min 12)
var cols=16;                                    // cols (min 12)
var dbug=false;                                 // debug mode
var nln='\n';                                   // line break
var markerShip='O';                             // board marker
var markerHit='X';                              // board marker
var markerValid='+';                            // board marker
var markerWater='.';                            // board marker
var markerEmpty=' ';                            // board marker
var cmp=true;                                   // computer player?
var lvl=1;                                      // cleverness (0:random,1:aggressive)
var ships=new Array(5,4,4,3,3,3,2,2,2,2);       // ships (length)
var remsh=new Array();                          // remaining enemy ships
var mxtry=10;                                   // max tries to place a ship
var btnaud='';                                  // button audio
var clkaud='';                                  // color button audio
var erraud='';                                  // error audio



/* add class to element */
function addClassToElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  if(cls.includes(elmcls)==false){
    var newcls=cls.concat(' '+elmcls);
    document.getElementById(elmid).className=newcls;
  }
}

/* remove class from element */
function removeClassFromElementById(elmid,elmcls){
  var cls=getClassStringForElementById(elmid);
  var newcls=cls.split(elmcls).join('');
  document.getElementById(elmid).className=newcls;
}

/* generate box id for click */
function getBoxIdForRowAndCol(r,c){
  return 'box-'+r+'-'+c;
}

/* get the string with class names for element */
function getClassStringForElementById(elmid){
  return document.getElementById(elmid).className;
}

/* do click on element by id */
function clickOnElementById(elmid){
  document.getElementById(elmid).click();
}

/* hide cover layer */
function hideCover(){
  addClassToElementById('cover1','cover1hide');
  addClassToElementById('cover2','cover2hide');
}

/* hide second screen for certain conditions */
function hideSecondScreen(){
  if(cmp==false){
    addClassToElementById('mygrid','elemhide');
    addClassToElementById('ships','elemhide');
  }
}

/* center game */
function centerWrapper(){
  var left=Math.floor((document.documentElement.clientWidth-document.getElementById('wrapper').offsetWidth)/2);
  document.getElementById('wrapper').style.marginLeft=left+'px';
  var top=Math.floor((document.documentElement.clientHeight-document.getElementById('wrapper').offsetHeight)/2);
  document.getElementById('wrapper').style.marginTop=top+'px';
  var btn=document.getElementById('new').offsetWidth;
  var left=Math.floor((document.getElementById('wrapper').offsetWidth-(btn*3))/2)-45;
  document.getElementById('info').style.marginLeft=left+'px';
  var left=Math.floor(document.getElementById('grid').offsetWidth)+20;
  var top=Math.floor(document.getElementById('mygrid').offsetHeight)+10;
  document.getElementById('ships').style.marginLeft=left+'px';
  document.getElementById('ships').style.marginTop=top+'px';
}

/* get start and end position in string for regex */
function getPositionForRegEx(str,regex,dir='>') {
  var pos=-1;
  var rex=new RegExp(regex);
  var match=rex.exec(str);
  if(match!==null){
    if(dir=='>'){
      pos=match.index;
    }else{
      pos=(match.index+match[0].length)-1;
    }
  }
  return pos;
}

/* get color name for player */
function getColorNameForPlayer(plrflg){
  if(plrflg){return 'pl1';}
  return 'pl2';
}



/* generate dynamic game grids for players */
function generateGridsForPlayers(){
  console.log('generate grids for players');
  for(p=0;p<2;p++){
    var pgrid=new Array();
    for(r=0;r<rows;r++){
      pgrid[r]=new Array();
      for(c=0;c<cols;c++){
        pgrid[r][c]=markerEmpty;
      }
    }
    grid[p]=pgrid;
  }
}

/* reset grid for player */
function resetGridForPlayer(plrflg){
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      grid[Number(plrflg)][r][c]=markerEmpty;
    }
  }
}

/* which grid to set grid html */
function whichGrid(){
  if(plr==true||cmp==false){return 'grid';}
  return 'mygrid';
}

/* refresh grid */
function refreshGrid(){
  console.log('refresh grid');
  setGridHtml(generateGridHtml());
  if(cmp==true){
    togglePlayer();
    setGridHtml(generateGridHtml());
    togglePlayer();
  }
  if(plr==true){
    setRemainingShipList();
    setShipListHtml(generateShipListHtml());
  }
}

/* generate grid html */
function generateGridHtml(){
  console.log('generate grid html');
  // setup
  var html='';
  // dynamic grid
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      // show colors for stones
      var colcls='';
      if(parseInt(grid[Number(!plr)][r][c])>0){colcls+=' ship';}
      if(dbug==true&&parseInt(grid[Number(!plr)][r][c])>0){colcls+=' hint';}
      if(grid[Number(!plr)][r][c]==markerHit){colcls+=' '+getColorNameForPlayer(plr);}
      if(grid[Number(!plr)][r][c]==markerWater){colcls+=' water';}
      // show grid content
      var str='';
      if(dbug==true){str=grid[Number(!plr)][r][c];}
      // unique boxid only for large 'grid'
      var idstr='';
      if(whichGrid()=='grid'){idstr=' id="'+getBoxIdForRowAndCol(r,c)+'"';}
      // set html 'box'
      html+='        <div class="box'+colcls+'"'+idstr+' attr-row="'+r+'" attr-col="'+c+'">'+str+'</div>'+nln;
    }
    // clear row
    html+='        <div class="clear"></div>'+nln;
  }
  // return
  return html;
}

/* set grid html */
function setGridHtml(html){
  console.log('set grid html');
  document.getElementById(whichGrid()).innerHTML=html;
}

/* set ships on player grid */
function setShips(plrflg){
  console.log('set ships for player: '+getColorNameForPlayer(plrflg));
  // reset grid for player
  resetGridForPlayer(plrflg);
  // set ships on grid
  // - try setting ship
  // - if impossible repeat all for player
  // - or next ship
  var shpcnt=0;
  while(shpcnt<ships.length){
    var ship=ships[shpcnt];
    if(setShip(ship,plrflg)==false){
      setShips(plrflg);
      return false;
    }else{
      shpcnt++;
    }
  }
  // clear grid from 'valid' markers
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      if(grid[Number(plrflg)][r][c]==markerValid){grid[Number(plrflg)][r][c]=markerEmpty;}
    }
  }
  // return
  return true;
}

/* set ship for player */
function setShip(ship,plrflg){
  // setup
  var regEx='['+markerEmpty+']{'+(parseInt(ship)+2)+'}';
  var pos=-1;
  var cnt=0;
  // to find a place
  // - horizontal or vertical
  // - with enough space
  // - get position
  // - set ship a bit random
  // - surround with markers to create space
  while(pos<0&&cnt<mxtry){
    var hori=((Math.random()*1)<0.5)?true:false;
    if(hori==true){
      var r=Math.floor(Math.random()*rows);
      var line='';for(c=0;c<cols;c++){line+=grid[Number(plrflg)][r][c];}
      pos=getPositionForRegEx(line,regEx);
      if(pos==0){pos=Math.floor(Math.random()*parseInt(ship))+1;}
      if(pos>=0){
        for(s=0;s<parseInt(ship);s++){
          grid[Number(plrflg)][parseInt(r)][parseInt(pos+s)]=parseInt(ship);
          if(parseInt(r-1)>=0&&grid[Number(plrflg)][parseInt(r-1)][parseInt(pos+s)]==markerEmpty){grid[Number(plrflg)][parseInt(r-1)][parseInt(pos+s)]=markerValid;}
          if(parseInt(r+1)<rows&&grid[Number(plrflg)][parseInt(r+1)][parseInt(pos+s)]==markerEmpty){grid[Number(plrflg)][parseInt(r+1)][parseInt(pos+s)]=markerValid;}
        }
        if(parseInt(pos-1)>=0){grid[Number(plrflg)][parseInt(r)][parseInt(pos-1)]=markerValid;}
        if(parseInt(pos+s)<cols){grid[Number(plrflg)][parseInt(r)][parseInt(pos+s)]=markerValid};
      }
    }else{
      var c=Math.floor(Math.random()*cols);
      var line='';for(r=0;r<rows;r++){line+=grid[Number(plrflg)][r][c];}
      pos=getPositionForRegEx(line,regEx);
      if(pos==0){pos=Math.floor(Math.random()*parseInt(ship))+1;}
      if(pos>=0){
        for(s=0;s<parseInt(ship);s++){
          grid[Number(plrflg)][parseInt(pos+s)][parseInt(c)]=parseInt(ship);
          if(parseInt(c-1)>=0&&grid[Number(plrflg)][parseInt(pos+s)][parseInt(c-1)]==markerEmpty){grid[Number(plrflg)][parseInt(pos+s)][parseInt(c-1)]=markerValid;}
          if(parseInt(c+1)<cols&&grid[Number(plrflg)][parseInt(pos+s)][parseInt(c+1)]==markerEmpty){grid[Number(plrflg)][parseInt(pos+s)][parseInt(c+1)]=markerValid;}
        }
        if(parseInt(pos-1)>=0){grid[Number(plrflg)][parseInt(pos-1)][parseInt(c)]=markerValid;}
        if(parseInt(pos+s)<rows){grid[Number(plrflg)][parseInt(pos+s)][parseInt(c)]=markerValid;}
      }
    }
    cnt++;
  }
  // return
  if(pos>=0){return true;}
  return false;
}

/* set remaining ship list */
function setRemainingShipList(){
  // setup
  remsh=new Array();
  // all ships
  ships.forEach(function(num){
    // regex to find
    var regex='['+num+']{'+num+'}';
    // prepare regex
    var rex=new RegExp(regex,'g');
    // counter for found ships
    var cnt=0;
    // horizontal
    for(r=0;r<rows;r++){
      var line='';for(c=0;c<cols;c++){line+=grid[Number(!plr)][r][c];}
      cnt+=getMatchAmount(rex,line);
    }
    // vertical
    for(c=0;c<cols;c++){
      var line='';for(r=0;r<rows;r++){line+=grid[Number(!plr)][r][c];}
      cnt+=getMatchAmount(rex,line);
    }
    // add found ships to list
    remsh[num]=cnt;
  });
}

/* get amount of found matches */
function getMatchAmount(rex,str){
  var matches=Array.from(str.matchAll(rex));
  return matches.length;
}

/* generate ship list html */
function generateShipListHtml(){
  console.log('generate ship list');
  // setup
  var html='';
  html+='        <div class="shipshead">Enemyships</div>';
  // all ships
  ships.forEach(function(num){
    // hide/show remaining ships
    var cls=' elemhide';
    if(remsh[num]>0){remsh[num]-=1;cls='';}
    // create dots
    var dots='';for(x=0;x<num;x++){dots+='<div class="shdot"></div>';}
    html+='        <div class="shlen len'+num+cls+'">'+dots+'</div>';
  });
  // return html
  return html;
}

/* set ship list html */
function setShipListHtml(html){
  console.log('set ship list html');
  document.getElementById('ships').innerHTML=html;
}

function toggleShipList(){
  var clsstr=getClassStringForElementById('ships');
  if(clsstr.includes('elemhide')==true){
    removeClassFromElementById('ships','elemhide');
  }else{
    addClassToElementById('ships','elemhide');
  }
}

function toggleReveal(){
  dbug=!dbug;
  refreshGrid();
}



/* set everything for click */
function setForClick(r,c){
  console.log('set for click')
  // setup
  var r=parseInt(r);
  var c=parseInt(c);
  // set marker
  if(grid[Number(!plr)][r][c]==markerEmpty){grid[Number(!plr)][r][c]=markerWater;}
  if(parseInt(grid[Number(!plr)][r][c])>0){grid[Number(!plr)][r][c]=markerHit;}
}

/* is box clickable on visible 'grid' */
function isClickable(boxid){
  var cls=getClassStringForElementById(boxid);
  if(cls.includes('water')==false&&cls.includes('pl1')==false&&cls.includes('pl2')==false){return true;}
  return false;
}

/* is box on opponent grid possible to click */
function isPossibleToClick(r,c){
  // setup
  var r=parseInt(r);
  var c=parseInt(c);
  // check if possible on opponent grid
  var gmark=grid[Number(!plr)][r][c];
  if(gmark!=markerWater&&gmark!=markerHit){return true;}
  return false;
}



/* next player: check possible moves (0:toggle back) */
function nextPlayer(){
  togglePlayer();
  refreshGrid()
}

/* toggle player */
function togglePlayer(){
  // remove all possible colors
  removeClassFromElementById('plr','pl1');
  removeClassFromElementById('plr','pl2');
  removeClassFromElementById('plr','ovr');
  // switch and set color for player
  plr=!plr;
  setPlayerColorMarker();
}

/* set color marker for player */
function setPlayerColorMarker(){
  addClassToElementById('plr',getColorNameForPlayer(plr));
}

/* are there still ships to hit */
function getPossibleMovesAmount(){
  // setup
  var amt=0;
  // check for places with 'ship' (>0)
  for(r=0;r<rows;r++){
    for(c=0;c<cols;c++){
      if(grid[Number(!plr)][r][c]>0){amt++;}
    }
  }
  // return
  return amt;
}



/* did player win or not */
function checkForEndGame(){
  console.log('check for end game');
  // no possible moves for player
  if(getPossibleMovesAmount()==0){endGame();}
}

/* end of game */
function endGame(){
  console.log('game ended');
  erraud.play();
  // set flag
  ovr=true;
  // last refresh
  refreshGrid();
  // set color
  addClassToElementById('grid','win-'+getColorNameForPlayer(plr));
}

/* continue game if not over */
function continueGame(){
  if(ovr==false){
    // toggle player
    nextPlayer();
    // opponent (false) can be a computer player
    if(cmp==true&&plr==false){computerMove();}
  }
}



/* computer player is making a move */
function computerMove(){
  console.log('compuer move');
  // how clever should the computer react
  var boxid='';
  switch(lvl) {
    case 1:
      boxid=computerMoveAggressive();
      break;
    case 0:
    default:
      boxid=computerMoveRandom();
  }
  // nothing found, do a random one
  if(boxid==''){boxid=computerMoveRandom();}
  // do click
  setTimeout(()=>{document.getElementById(boxid).click();},200);
}

/* computer move to win */
function computerMoveAggressive(){
  var boxid='';
  // setup
  var regExLR='[^'+markerHit+''+markerWater+']{1}['+markerHit+']+';
  var regExRL='['+markerHit+']+[^'+markerHit+''+markerWater+']{1}';
  var pos=-1;
  // horizontal
  for(r=0;r<rows;r++){
    var line='';for(c=0;c<cols;c++){line+=grid[Number(!plr)][r][c];}
    pos=getPositionForRegEx(line,regExLR,'>');
    if(pos<0){pos=getPositionForRegEx(line,regExRL,'<');}
    if(pos>=0){boxid=getBoxIdForRowAndCol(r,pos);}
  }
  // vertical
  if(pos==-1){
    for(c=0;c<cols;c++){
      var line='';for(r=0;r<rows;r++){line+=grid[Number(!plr)][r][c];}
      pos=getPositionForRegEx(line,regExLR,'>');
      if(pos<0){pos=getPositionForRegEx(line,regExRL,'<');}
      if(pos>=0){boxid=getBoxIdForRowAndCol(pos,c);}
    }
  }
  // return
  return boxid;
}

/* computer move random */
function computerMoveRandom(){
  // prepare
  var boxid='';
  // select a random clickable move
  while(boxid.length==0){
    var r=Math.floor(Math.random()*rows);
    var c=Math.floor(Math.random()*cols);
    if(isPossibleToClick(r,c)==true){boxid=getBoxIdForRowAndCol(r,c);break;}
  }
  // return
  return boxid;
}



/* setup game audio files */
function setupAudio(){
  btnaud=new Audio('button.wav');
  btnaud.volume=0.15;
  clkaud=new Audio('click.wav');
  clkaud.volume=0.5;
  erraud=new Audio('error.wav');
  erraud.volume=0.5;
}



/* init game */
function init(){
  console.log('init');
  console.log('cmp: '+cmp);
  console.log('lvl: '+lvl);
  // fresh start
  setupAudio();
  hideSecondScreen();
  grid=new Array();
  generateGridsForPlayers();
  setShips(plr);
  setShips(!plr);
  plr=false;
  nextPlayer();
  ovr=false;
  removeClassFromElementById('grid','win-pl1');
  removeClassFromElementById('grid','win-pl2');
  // and go
  console.log('ready!');
}



/* listener for key */
document.addEventListener('keydown',(event)=>{
  switch(event.which){
    // h(elp)
    case 72:
      if(ovr==false){clickOnElementById('hnt');}
      break;
    // n(ew)
    case 78:
      if(ovr==true){clickOnElementById('new');}
      break;
    // r(eveal)
    case 82:
      if(ovr==false){toggleReveal();}
      break;
  }
},false);

/* listener for click */
document.addEventListener('click',(event)=>{
  // clicked on a button
  if(event.target.matches('.button')){
    btnaud.cloneNode(true).play();
    var action=event.target.getAttribute('attr-action');
    console.log('button: '+action);
    // what to do?
    if(ovr==true&&action=='new'){init();}
    if(ovr==false&&action=='hnt'){toggleShipList();}
  }
  // clicked on a box
	if(ovr==false&&event.target.matches('#grid>.box')){
    clkaud.cloneNode(true).play();
    var r=event.target.getAttribute('attr-row');
    var c=event.target.getAttribute('attr-col');
    // only clickable fields
    if(isPossibleToClick(r,c)==true){
      // set click
      setForClick(r,c);
      // check if game is over
      checkForEndGame();
      // only if two player mode
      if(cmp==false){refreshGrid();}
      // continue after some time
      var tms=(cmp==true)?200:2000;
      setTimeout(()=>{continueGame();},tms);
    }
  }
},false);



let stateCheck=setInterval(()=>{
  if(document.readyState=='complete'){
    clearInterval(stateCheck);
    init();
    centerWrapper();
    hideCover();
    setTimeout(()=>{addClassToElementById('wrapper','rotatein');},2500);
  }
},250);